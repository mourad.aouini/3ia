using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
public class testConnexion : MonoBehaviour
{
    public Texture2D imageTexture; // Assign the image texture in the Unity Editor
    public TextMeshProUGUI resultText; // Reference to the UI Text component where the result will be displayed

    IEnumerator UploadImage(string url)
    {
        // Ensure that an image is assigned
        if (imageTexture == null)
        {
            Debug.LogError("No image assigned!");
            yield break;
        }

        // Create a new form
        WWWForm form = new WWWForm();
        // Add the image data to the form
        form.AddBinaryData("file", imageTexture.EncodeToJPG(), "image.jpg", "image/jpeg");
        // Set headers
        UnityWebRequest www = UnityWebRequest.Post(url, form);
        // Set content type
        www.SetRequestHeader("accept", "application/json");

        // Send the request
        yield return www.SendWebRequest();

        // Check for errors
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError(www.error);
            // Display error message
            resultText.text = "Error: " + www.error;
        }
        else
        {
            Debug.Log("Image upload complete!");
            Debug.Log(www.result.ToString());
            // Display success message
            resultText.text = "Image upload complete!";
            // Display result text
            resultText.text += "\nResponse: " + www.downloadHandler.text;
        }
    }

    // Example usage
    void Start()
    {
        string url = "https://lx3qftpd-8000.euw.devtunnels.ms/yolo/";
        StartCoroutine(UploadImage(url));
    }
}
