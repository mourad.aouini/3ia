using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
public class ImageUploader : MonoBehaviour
{
    public RawImage cameraView; // Reference to the UI RawImage component where the camera view will be displayed
    public TextMeshProUGUI resulat; // Reference to the UI Text component where the result will be displayed

    WebCamTexture webcamTexture; // WebCamTexture to capture images from the camera

    void Start()
    {
        // Start the camera
        webcamTexture = new WebCamTexture();
        cameraView.texture = webcamTexture;
        webcamTexture.Play();
    }

    // Method to handle the upload of the current camera frame
    public void UploadOnClick(string url)
    {
        // Convert the current frame from the camera to a Texture2D
        Texture2D currentFrameTexture = TextureFromWebCam(webcamTexture);

        // Create a new form
        WWWForm form = new WWWForm();
        // Add the image data to the form
        byte[] imageBytes = currentFrameTexture.EncodeToJPG();
        form.AddBinaryData("file", imageBytes, "image.jpg", "image/jpeg");
        // Set headers
        UnityWebRequest www = UnityWebRequest.Post(url, form);
        // Set content type
        www.SetRequestHeader("accept", "application/json");

        // Send the request
        StartCoroutine(SendRequest(www));
    }

    // Coroutine to send the request and handle the response
    IEnumerator SendRequest(UnityWebRequest request)
    {
        // Send the request
        yield return request.SendWebRequest();

        // Check for errors
        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError(request.error);
            // Display error message
            resulat.text = "Error: " + request.error;
        }
        else
        {
            Debug.Log("Image upload complete!");
            Debug.Log(request.result.ToString());
            // Display success message
            resulat.text = "Image upload complete!";
            // Display result text
            resulat.text += "\nResponse: " + request.downloadHandler.text;
        }
    }

    // Convert WebCamTexture to Texture2D
    Texture2D TextureFromWebCam(WebCamTexture webcamTexture)
    {
        Texture2D tex = new Texture2D(webcamTexture.width, webcamTexture.height);
        tex.SetPixels(webcamTexture.GetPixels());
        tex.Apply();
        return tex;
    }
}
